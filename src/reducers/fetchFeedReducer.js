import {FETCH_FEED } from "../actions/types";

const INITIAL_STATE={
    feed:[]
}

 export const fetchFeedReducer = (state = INITIAL_STATE, action)=>{
    console.log('Red',action.payload);
    switch (action.type) {
        case FETCH_FEED:
            return {
                ...state,
                feed : action.payload
            }
       
        default:
            return state
    }
}