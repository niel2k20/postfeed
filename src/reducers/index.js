import { combineReducers } from 'redux';
import { reducer as formReducer } from "redux-form";
import {authReducer} from "./authReducer";
import {fetchFeedReducer} from './fetchFeedReducer';



export default combineReducers({
    auth: authReducer,
    form : formReducer,
    feed: fetchFeedReducer
})

