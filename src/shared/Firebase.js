import firebase from "firebase/app";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDEG8CN8-YjIxn8tDIImldqlruMMSe8UoE",
  authDomain: "postfeed-4744d.firebaseapp.com",
  databaseURL: "https://postfeed-4744d-default-rtdb.firebaseio.com",
  projectId: "postfeed-4744d",
  storageBucket: "postfeed-4744d.appspot.com",
  messagingSenderId: "839693060727",
  appId: "1:839693060727:web:048502935a428283dbd472",
  measurementId: "G-5WKM7KXYXG"
};
firebase.initializeApp(firebaseConfig);

export default firebase;
