import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import ListPosts from './components/ListPosts';
import CreatePost from './components/CreatePost';
import Header from './components/Header';
import history from './components/history';

class App extends Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <Header />
          <div>
            <Switch>
            <Route path='/' exact component={ListPosts} />
            <Route path='/new' exact component={CreatePost} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
