import { SIGN_IN, SIGN_OUT,FETCH_FEED } from './types';
import firebase from '../shared/Firebase';
import 'firebase/firestore';

export const trySignIn = (userId) => {
  // console.log('actions',userId);
  return {
    type: SIGN_IN,
    payload: userId,
  };
};

export const trySignOut = () => {
  return {
    type: SIGN_OUT,
  };
};

export const fetchfeed = () => {
  return async function (dispatch,getStore) {
    const feed=[]
    firebase.firestore().collection("feeds")
    .get()
    .then(snapshot=>{
     
      snapshot.forEach(doc=>{
        const data =doc.data()
        feed.push(data)
      })
      console.log('fssssss',feed);
      
      console.log('snapshot is',snapshot.docs);
    })
    .catch(error => console.log(error,'error'))

    dispatch({
      type: FETCH_FEED,
      payload:feed,
  });

      // dispatch({
      //     type: FETCH_FEED,
      //     payload: response.data,
      // });
  };
};
