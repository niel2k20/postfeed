export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const FETCH_FEED = 'FETCH_FEED';
export const UPLOAD_FEED = 'UPLOAD_FEED';
