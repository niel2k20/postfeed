import React, { Component } from 'react';
import firebase from '../shared/Firebase';
import 'firebase/firestore';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class ListPosts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feed: []
    };
  }

  componentDidMount = () => {
    //  const response = firebase.firestore().collection("feeds");
    //  console.log('ZZZZZZZZZZZZZZZZZZZ',response);

    if (this.props.auth) {
      firebase
      .firestore()
      .collection('feeds')
      .get()
      .then((snapshot) => {
        const feed = [];
        snapshot.forEach((doc) => {
          const data = doc.data();
          feed.push(data);
        });
        this.setState({
          feed: feed,
        });
        console.log('snapshot is', snapshot.docs);
      })
      .catch((error) => console.log(error, 'error'));
    }
    // this.props.fetchfeed();
  }
  render() {
    // console.log('RRRRRRRRRRRRRRR', this.state.feed);
    return (
      <div>
        {this.props.auth===null? <p>please Signin</p>:''}
      {this.state.feed.map((feed, index) => (
          <div className='ui feed segment'>
            <div className='event'>
              <div className='label'>
                <img
                  src={'https://semantic-ui.com/images/avatar/small/elliot.jpg'}
                />
              </div>
              <div className='content'>
                <div className='summary'>
                  <a>{feed.username}</a> posted
                </div>
                <div className='extra text'>Title: {feed.title}</div>

                <div className='extra text'>Description: {feed.description}</div>
                <div className='extra images'>
                  <a>
                    <img
                    alt={feed.title}
                      src={feed.file}
                      onClick={() => window.open(feed.file, '_blank')}
                    />
                  </a>
                  
                </div>
              </div>
            </div>
            <div class='ui divider'></div>
          </div>
        ))}
        {/* <Link to='/new' className='ui button primary'>Create new</Link> */}
        <div>
        <Link to='/new' className='ui button primary'>Create new</Link> 
        </div>
      </div>
    )
  }
}
function mapStateToProps({auth}) {
  console.log('state.auth',auth);
  return {
auth:auth.userData
  };
}

 

export default connect(mapStateToProps,null)(ListPosts)
