import React, { Component } from 'react';
import GoogleAuth from './GoogleAuth';

class Header extends Component {
  render() {
    return (
      <div className='ui menu'>
        <p className=' header item'>POST FEED</p>
        <div className='right menu'>
          <div className='header item'>
            <GoogleAuth />
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
