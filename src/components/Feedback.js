import React, { Component } from 'react' 
import firebase from './Firebase';
import 'firebase/firestore';


export default class FeedBack extends Component {
    constructor() {
        super();

        this.state = {
            message: ''
        };
    }
    ratingChanged(nextValue, prevValue, name) {
        this.setState({ rating: nextValue });
    }
    onTextChange = (value) => {

        console.log(value);
    }
    onFormSubmit = (e) => {
        e.preventDefault(); 
        this.setState({
             
            message: ''
        })
        firebase.firestore().collection("contacts")
            .add({ 
                message: this.state.message
            })
            .then(() => {

                this.setState({ 
                    message: ''
                });
                alert("Your message has been submitted👍");
            })
            .catch((error) => {
                alert(error.message);
            });


    }

    render() {
        return (
            <div className="ui two column centered grid">
                <div className="column">
                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                        <label>Rating</label>
                         
                        <div className="ui form">
                            <div className="field">
                                <label>Comments</label>
                                <textarea rows={2}
                                    value={this.state.message}
                                    onChange={(e) => this.setState({ message: e.target.value })}
                                />
                            </div>
                        </div><br />
                        <button className="ui primary button">
                            Submit
                </button>
                    </form>
                </div>
            </div>


        );
    };
}
