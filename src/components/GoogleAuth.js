import React, { Component } from 'react';
import { trySignIn, trySignOut } from '../actions';
import { connect } from 'react-redux';

class GoogleAuth extends Component {
  state = { isSignedIn: null };
  componentDidMount() {
    window.gapi.load('client:auth2', () => {
      window.gapi.client
        .init({
          clientId:
            '17911862683-5fvtlb45go6ul30anvf9omvgle0rpmqo.apps.googleusercontent.com',
          scope: 'email profile',
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();
        //   console.log('then 1', this.auth);
          // this.setState({ isSignedIn: this.auth.isSignedIn.get() });
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  renderAuthButton() {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <button onClick={this.onSignOutClick} className='ui google red button'>
          <i className='google icon' />
          Log out
        </button>
      );
    } else {
      return (
        <button onClick={this.onSignInClick} className='ui google red button'>
          <i className='google icon' />
          Login with Google
        </button>
      );
    }
  }

  onAuthChange = (isSignedIn) => {
    // console.log('urrentUser', this.auth.currentUser.ee.Es);
    const data={
        name:this.auth.currentUser.get().getBasicProfile().getName(),
        email:this.auth.currentUser.get().getBasicProfile().getEmail(),
        userId:this.auth.currentUser.get().getBasicProfile().getId()

    }
    console.log('safasfafs', this.auth.currentUser.get().getBasicProfile());
    if (isSignedIn) {
      //   this.props.trySignIn(this.auth.currentUser.get().getId());
      this.props.trySignIn(data);
    } else {
      this.props.trySignOut();
    }
  };
  onSignInClick = () => {
    this.auth.signIn();
  };
  onSignOutClick = () => {
    this.auth.signOut();
  };

  render() {
    return <div>{this.renderAuthButton()}</div>;
  }
}

const mapStateToProps = (state) => {
  // console.log('state is ',state);
  return {
    isSignedIn: state.auth.isSignedIn,
  };
};

export default connect(mapStateToProps, { trySignIn, trySignOut })(GoogleAuth);
