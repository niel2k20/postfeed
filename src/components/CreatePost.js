import React, { Component } from 'react';
import firebase from '../shared/Firebase';
import 'firebase/firestore';
import history from './history';
import { connect } from 'react-redux';
class CreatePost extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      image: '',
      fileUrl: '',
    };
  }

  async onFormSubmit(event) {
    event.preventDefault();
    console.log('Title is', this.state.title);
    console.log('Description is', this.state.description);
    console.log('image is', this.state.image);

    // await this.fileUplaod();
    if (this.state.title && this.state.description && this.state.image && this.props.auth) {
        await this.fileUplaod();
    }
  }

  feedPost(){
    //   console.log('asf');
      firebase.firestore().collection("feeds")
            .add({
               title:this.state.title,
               description: this.state.description,
               userId:this.props.auth.userId,
               file:this.state.fileUrl,
               username:this.props.auth.name


            })
            .then(() => {

               console.log('asdasd',history);
               window.location.href = '/';
            //    history.push('/')
                // alert("Your message has been submitted👍");
            })
            .catch((error) => {
                alert(error.message);
            });

            // console.log('afasfada',this.abc);


  }
  
  fileUplaod() {
    
      let bucketName = 'images';
      let file = this.state.image;
      let storageRef = firebase.storage().ref(`${bucketName}/${file.name}`);
      storageRef.put(file).on(firebase.storage.TaskEvent.STATE_CHANGED, () => {
        this.showImage();
      });
     
  }
  showImage() {
    firebase
      .storage()
      .ref()
      .child('images/' + this.state.image.name)
      .getDownloadURL()
      .then((url) => {
        console.log('safagagsgdasg', url);
        this.setState({
          fileUrl: url,
        });
        this.feedPost()
      });
  }
  render() {
    return (
      <div className='ui segment'>
        <form className='ui form' onSubmit={(e) => this.onFormSubmit(e)}>
          <div className='field'>
            <label>Title</label>
            <input
              type='text'
              name='title'
              placeholder='Enter Title'
              onChange={(e) => this.setState({ title: e.target.value })}
            />
          </div>
          <div className='field'>
            <label>Description</label>
            <input
              type='text'
              name='description'
              placeholder='Enter Description'
              onChange={(e) => this.setState({ description: e.target.value })}
            />
          </div>
          <div className='field'>
            <label>Last Name</label>
            <input
              type='file'
              name='image'
              onChange={(e) => this.setState({ image: e.target.files[0] })}
            />
          </div>
          <button className='ui button' type='submit'>
            Submit
          </button>
        </form>

        {/* Create Post Works */}
      </div>
    );
  }
}

const mapStateToProps=({auth})=>{
    console.log('assf',auth.userData);
    return{
        auth : auth.userData
    }
}

export default connect(mapStateToProps,null)(CreatePost);
